# CEAS Archetype Frmn IOS VIPER

Proyecto arquetipo para desarrollos front móvil basado en IOS con el patrón de diseño VIPER

## Descripción general

Este arquetipo sirve como base para la generación de proyectos IOS, las funcionalidades básicas que el arquetipo maneja son:

- **Internacionalización**: configuración que permite el manejo de textos estáticos en archivos de recursos facilitando el uso de varios lenguajes.

- **Manejo de temas**: configuración que permite el cambio de tema dentro de la aplicación.

- **Compilación por entornos**: configuración que permite crear diferentes compilaciones a partir del mismo proyecto.

- **Consumo de servicios Backend**: clase que permite el consumo de servicios Back y el manejo de excepciones.

- **Seguridad**: Clases que contemplan varios aspectos de seguridad dentro de la aplicación.

- **Visualización en segundo plano**: Se oculta la imagen de la aplicación en segundo plano.

- **Dispositivos rooteados**: Se detectan los dispositivos root, se notificará al usuario la detección de root y se cerrará la aplicación

- **Analytics**: metodos que permiten el registro de eventos a través de la plataforma Google analytics.

- **Notificaciones**:  Se hace mención a la forma en como se debe implementar la notificaciones por medio de Firebase.

- **Geolocalización**: Clases que habilitan la geolocalización dentro del dispositivo siempre que se requiera la ubicación del usuario.

## Prerrequisitos

Para la instalación y uso del arquetipo, el desarrollador deberá contar con el XCode y por dichos motivos se debe contar con un dispositivo MAC para el desarrollo en IOS. 

## Ejecución para ambientes

Una vez el desarrollador cuente con el código del arquetipo, al compilar el código en XCode se permitirá seleccionar el entorno deseado, se deberá seleccionar y este tomara los valores descritos en los archivos .xconfig para la compilación del proyecto.