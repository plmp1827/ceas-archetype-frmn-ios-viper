//
//  ConfigUtil.swift
//  cibanco_arquetipo_ios
//
//  Created by Digital on 01/03/22.
//

import Foundation

struct ConfigUtil{
    
    static func getBaseUrl()->String{
        return  ProcessInfo.processInfo.environment["URL_SERVICE"] ?? ""
    }
    
}
