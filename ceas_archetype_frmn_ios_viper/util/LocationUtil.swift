//
//  LocationUtil.swift
//  cibanco_arquetipo_ios
//
//  Created by Digital on 27/02/22.
//

import Foundation
import CoreLocation
import Combine
 
class LocationUtil: NSObject, ObservableObject, CLLocationManagerDelegate {

    private let locationManager = CLLocationManager()
    @Published var locationStatus: CLAuthorizationStatus?
    @Published var lastLocation: CLLocation?

    override init() {
        super.init()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }

    var statusString: String {
        guard let status = locationStatus else {
            return Constants.unknown
        }
        switch status {
        case .notDetermined: return Constants.notDetermined
        case .authorizedWhenInUse: return Constants.authorizedWhenInUse
        case .authorizedAlways: return Constants.authorizedAlways
        case .restricted: return  Constants.restricted
        case .denied: return Constants.denied
        default: return Constants.unknown
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        locationStatus = status
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        lastLocation = location
    }
}
