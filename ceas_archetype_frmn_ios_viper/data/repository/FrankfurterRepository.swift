//
//  FrankfurterRepository.swift
//  cibanco_arquetipo_ios
//
//  Created by Digital on 01/03/22.
//

import Foundation
import Combine
import SwiftUI

struct FrankfurterRepository{
 
    static func getRatesEUR() -> AnyPublisher<FrankfurterEntity,APIError> {
        let baseUrl:String = ConfigUtil.getBaseUrl()
        let frankFurter:AnyPublisher<FrankfurterEntity, APIError> = HttpsUrlConnectionCIBanco.connect(headers: [:], url: "\(baseUrl)/latest",httpMethod: HttpsMethod.GET)
        return frankFurter
    }
        
    
    
}
