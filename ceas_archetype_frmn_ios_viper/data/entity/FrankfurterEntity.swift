//
//  FrankfurterEntity.swift
//  cibanco_arquetipo_ios
//
//  Created by Digital on 01/03/22.
//

import Foundation

struct FrankfurterEntity : Codable{
    let amount: Double?
    let base: String?
    let date: String?
    let rates: RatesEntity?
}
