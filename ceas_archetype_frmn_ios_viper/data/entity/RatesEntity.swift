//
//  RatesEntity.swift
//  cibanco_arquetipo_ios
//
//  Created by Digital on 01/03/22.
//

import Foundation

struct RatesEntity : Codable{
    let AUD: Double
    let BGN: Double
    let BRL: Double
    let CAD: Double
    let CHF: Double
    let CNY: Double
    let CZK: Double
    let DKK: Double
    let GBP: Double
    let HKD: Double
    let HRK: Double
    let HUF: Double
    let IDR: Double
    let ISK: Double
    let JPY: Double
    let KRW: Double
    let MXN: Double
    let MYR: Double
    
    
    public var description:String{
        return "aud: \(AUD),\nbgn: \(BGN),\nbrl: \(BRL),\ncad: \(CAD),\nchf: \(CHF),\ncny: \(CNY),\nczk: \(CZK),\ndkk: \(DKK),\ngbp: \(GBP),\nhkd: \(HKD),\nhrk: \(HRK),\nhuf: \(HUF),\nidr: \(IDR),\nisk: \(ISK),\njpy: \(JPY),\nkrw: \(KRW),\nmxn: \(MXN),\nmyr: \(MYR).\n"
    }
}
