//
//  UrlConnectionCIBanco.swift
//  cibanco_arquetipo_ios
//
//  Created by Digital on 01/03/22.
//

import Foundation
import Combine
import SwiftUI

struct HttpsUrlConnectionCIBanco{
    
    static func connect<T: Decodable>(headers:Dictionary<String,String>, url: String, _ decoder: JSONDecoder = JSONDecoder(),body:Any = "" as Any,httpMethod:HttpsMethod) -> AnyPublisher<T, APIError> {
        
            var request = URLRequest(url: URL(string: url)!)
            let method:String = httpMethod.rawValue
            request.httpMethod = method
            if(method == "POST" || method == "PUT"){
                request.httpBody = try?JSONSerialization.data(withJSONObject: body,options: [])
            }
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            if(!headers.isEmpty){
                for(key,value) in headers{
                    request.allHTTPHeaderFields = [key:value]
                }
            }
            return URLSession.shared
             .dataTaskPublisher(for: request)
             .mapError{ APIError.serverError(code: $0.errorCode,
                                             message: $0.localizedDescription) }
             .map { $0.data }
             .decode(type: T.self, decoder: JSONDecoder())
             .mapError { _ in APIError.decodingError }
             .receive(on: DispatchQueue.main)
             .eraseToAnyPublisher()
        }
    
}

public enum APIError: Error {
    case internalError
    case decodingError
    case serverError(code: Int, message: String)
}

public enum HttpsMethod:String{
    case PUT = "PUT"
    case POST = "POST"
    case GET = "GET"
    case DELETE = "DELETE"
}
