//
//  Constants.swift
//  cibanco_arquetipo_ios
//
//  Created by Digital on 23/02/22.
//

import Foundation

struct Constants{
    
    static let goApiView : String = "go_api_view"
    static let goGeoView : String = "go_geo_view"
    static let name : String = "name"
    static let fullText : String = "full_text"
    static let itemName : String = "item_name"
    static let apiName : String = "api_name"
    static let geoName : String = "geo_name"
    
    //status
    static let unknown : String = "unknown"
    static let notDetermined : String = "notDetermined"
    static let authorizedWhenInUse : String = "authorizedWhenInUse"
    static let authorizedAlways : String = "authorizedAlways"
    static let restricted : String = "restricted"
    static let denied : String = "denied"
    
}
