//
//  GeoView.swift
//  cibanco_arquetipo_ios
//
//  Created by Digital on 18/02/22.
//

import SwiftUI

struct GeoView: View {
    
    @ObservedObject var presenter:GeoPresenter
    @State private var lat: String = ""
    @State private var long: String = ""
    @State private var showToast = false
        
        var body: some View {
            VStack{
                    Text(Bundle.main.localizedString(forKey: "yourLocation", value: "yourLocation", table: "Localizable")).multilineTextAlignment(.leading).frame(width: UIScreen.main.bounds.width, height: 0, alignment: .leading).padding(EdgeInsets(top: 20.0, leading: 40.0, bottom: 0.0, trailing: 0.0))
                Text(Bundle.main.localizedString(forKey: "latitude", value: "latitude", table: "Localizable")+" \(lat)").multilineTextAlignment(.leading).frame(width: UIScreen.main.bounds.width, height: 0, alignment: .leading).padding(EdgeInsets(top: 20.0, leading: 40.0, bottom: 0.0, trailing: 0.0))
                Text(Bundle.main.localizedString(forKey: "longitude", value: "longitude", table: "Localizable")+" \(long)").multilineTextAlignment(.leading).frame(width: UIScreen.main.bounds.width, height: 0, alignment: .leading).padding(EdgeInsets(top: 20.0, leading: 40.0, bottom: 50.0, trailing: 0.0))
                Button(action: {
                    let isOkeyService = presenter.checkStatus()
                    if(isOkeyService){
                        let lm:LocationUtil = presenter.getlocation()
                        lat = "\(lm.lastLocation?.coordinate.latitude ?? 0)"
                        long = "\(lm.lastLocation?.coordinate.longitude ?? 0)"
                    }else{
                        showToast.toggle()
                    }
                }){
                    HStack {
                        Spacer()
                        Text(Bundle.main.localizedString(forKey: "getLocation", value: "getLocation", table: "Localizable")).foregroundColor(Color.white).bold()
                        Spacer()
                    }
                }
                .padding(EdgeInsets(top: 20.0, leading: 20.0, bottom: 20.0, trailing: 20.0))
                .background(Color(UIColor(named: "pantone356c")!))
                .cornerRadius(8.0)
                    Spacer()
            }.alert(isPresented: $showToast){
                Alert(title: Text(Bundle.main.localizedString(forKey: "location", value: "location", table: "Localizable")),message: Text(Bundle.main.localizedString(forKey: "requiresPermission", value: "requiresPermission", table: "Localizable")))
            }.padding()
        }
}

struct GeoView_Previews: PreviewProvider {
    static var previews: some View {
        return  NavigationView {

        }
    }
}
