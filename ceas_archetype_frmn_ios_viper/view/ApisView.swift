//
//  ApisView.swift
//  cibanco_arquetipo_ios
//
//  Created by Digital on 18/02/22.
//

import SwiftUI
import Combine

struct ApisView: View {
    
    @ObservedObject var presenter: ApiPresenter
        
    var body: some View {
        VStack{
            Text(Bundle.main.localizedString(forKey: "francfurtInfo", value: "francfurtInfo", table: "Localizable")).multilineTextAlignment(.leading).frame(width: UIScreen.main.bounds.width, height: 60, alignment: .leading).padding(EdgeInsets(top: 20.0, leading: 40.0, bottom: 0.0, trailing: 0.0)).fixedSize(horizontal: false, vertical: true)
            Text(Bundle.main.localizedString(forKey: "updateDate", value: "updateDate", table: "Localizable")+" \(presenter.frankfurter?.date ?? "")").multilineTextAlignment(.leading).frame(width: UIScreen.main.bounds.width, height: 0, alignment: .leading).padding(EdgeInsets(top: 0.0, leading: 40.0, bottom: 0.0, trailing: 0.0))
            Text(Bundle.main.localizedString(forKey: "mount", value: "mount", table: "Localizable")+" \(presenter.frankfurter?.amount ?? 0.0)"+" \(presenter.frankfurter?.base ?? "")").multilineTextAlignment(.leading).frame(width: UIScreen.main.bounds.width, height: 0, alignment: .leading).padding(EdgeInsets(top: 20.0, leading: 40.0, bottom: 0.0, trailing: 0.0))
            Text(Bundle.main.localizedString(forKey: "rate", value: "rate", table: "Localizable")).multilineTextAlignment(.leading).frame(width: UIScreen.main.bounds.width, height: 0, alignment: .leading).padding(EdgeInsets(top: 20.0, leading: 40.0, bottom: 0.0, trailing: 0.0))
            Text( "\(presenter.frankfurter?.rates?.description ?? "")").multilineTextAlignment(.leading).frame(width: UIScreen.main.bounds.width, height: 450, alignment: .leading).padding(EdgeInsets(top: 0.0, leading: 40.0, bottom: 50.0, trailing: 0.0)).fixedSize(horizontal: false, vertical: true)
            Spacer()
        }
        
    }
}

struct ApisView_Previews: PreviewProvider {
    static var previews: some View {
        let interactor = ApiInteractor()
        let presenter = ApiPresenter(interactor: interactor)
        ApisView(presenter:presenter)
    }
}
