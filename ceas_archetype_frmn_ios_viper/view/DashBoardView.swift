//
//  DashBoardView.swift
//  cibanco_arquetipo_ios
//
//  Created by Digital on 18/02/22.
//

import SwiftUI
import Firebase

struct DashBoardView: View {
    
    @State var apiSelection: Int? = nil
    @State var geoSelection: Int? = nil
    @State private var showAlert = false
    @ObservedObject var presenter:DashBoardPresenter
    
    var body: some View {
            NavigationView{
                VStack {
                    Image("cibanco").resizable().scaledToFit().padding(EdgeInsets(top: 0.0, leading: 20.0, bottom: 20.0, trailing: 20.0))
                    
                    Text(Bundle.main.localizedString(forKey: "functionalities", value: "functionalities", table: "Localizable")).multilineTextAlignment(.leading).frame(width: UIScreen.main.bounds.width, height: 0, alignment: .leading).padding(EdgeInsets(top: 0.0, leading: 40.0, bottom: 0.0, trailing: 0.0))
                    
                    NavigationLink(destination: presenter.clickApiView(), tag: 1, selection: $apiSelection) {
                        Button(action: {
                            self.apiSelection = 1
                            Analytics.logEvent(Constants.goApiView, parameters: [
                                Constants.name : Constants.itemName,
                                Constants.fullText : Constants.apiName
                            ])
                        }) {
                            HStack {
                                Spacer()
                                Text(Bundle.main.localizedString(forKey: "api", value: "api", table: "Localizable")).foregroundColor(Color.white).bold()
                                Spacer()
                            }
                        }
                        .padding()
                        .background(Color(UIColor(named: "pantone356c")!))
                        .cornerRadius(8.0)
                    }.padding(EdgeInsets(top: 20.0, leading: 20.0, bottom: 0.0, trailing: 20.0))
                    NavigationLink(destination: presenter.clickGeoView(), tag: 2, selection: $geoSelection) {
                        Button(action: {
                            self.geoSelection = 2
                            Analytics.logEvent(Constants.goGeoView, parameters: [
                                Constants.name : Constants.itemName ,
                              Constants.fullText : Constants.geoName
                            ])
                        }) {
                            HStack {
                                Spacer()
                                Text(Bundle.main.localizedString(forKey: "geo", value: "geo", table: "Localizable")).foregroundColor(Color.white).bold()
                                Spacer()
                            }
                        }
                        .padding()
                        .background(Color(UIColor(named: "pantone356c")!))
                        .cornerRadius(8.0)
                    }.padding(EdgeInsets(top: 10.0, leading: 20.0, bottom: 0.0, trailing: 20.0))
                    Spacer()
                }.navigationTitle("").navigationBarTitleDisplayMode(.inline).navigationBarItems(leading:HStack{
                    Text(Bundle.main.localizedString(forKey: "app_name", value: "app_name", table: "Localizable")).bold()
                }, trailing:HStack{}).alert(Bundle.main.localizedString(forKey: "rootDevice", value: "rootDevice", table: "Localizable"),isPresented: $showAlert){
                    Button(Bundle.main.localizedString(forKey: "okey", value: "okey", table: "Localizable")){
                        exit(0)
                    }
                }
            }.onReceive(NotificationCenter.default.publisher(for: UIApplication.didBecomeActiveNotification)) { _ in
                if(UIDevice().isJailBroken){
                    showAlert = true
                }
            }
    }
}

struct DashBoardView_Previews: PreviewProvider {
    static var previews: some View {
        let presenter = DashBoardPresenter()
        return DashBoardView(presenter: presenter)
    }
}
