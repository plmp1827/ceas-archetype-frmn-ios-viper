//
//  GeoPresenter.swift
//  cibanco_arquetipo_ios
//
//  Created by Digital on 24/02/22.
//

import SwiftUI

class GeoPresenter: ObservableObject{
    
    private let interactor:GeoInteractor
    
    init(interactor: GeoInteractor){
        self.interactor = interactor
    }
    
    func getlocation() -> LocationUtil{
        return interactor.getLocation()
    }
    
    func checkStatus()->Bool{
        let status = interactor.getStatus()
        if(status == Constants.authorizedAlways || status == Constants.authorizedWhenInUse){
            return true
        }else{
            return false
        }
    }
    
}
