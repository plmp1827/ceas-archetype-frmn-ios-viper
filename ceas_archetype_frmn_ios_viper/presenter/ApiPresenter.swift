//
//  APIPresenter.swift
//  cibanco_arquetipo_ios
//
//  Created by Digital on 01/03/22.
//

import Foundation
import Combine

class ApiPresenter : ObservableObject{
    
    let interactor: ApiInteractor
    private var cancellables = Set<AnyCancellable>()
    @Published var frankfurter:FrankfurterEntity? = nil
    
    init(interactor: ApiInteractor){
        self.interactor = interactor
        interactor.fran.sink {
            completion in
            switch completion {
            case .failure(_):
                break
            case .finished:
                break
            }
        } receiveValue: {
            value in
            self.frankfurter = value
        }.store(in: &self.cancellables) 
        
    }

    
}
