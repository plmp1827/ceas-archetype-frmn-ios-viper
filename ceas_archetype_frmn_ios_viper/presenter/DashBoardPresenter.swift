//
//  DashBoardPresenter.swift
//  cibanco_arquetipo_ios
//
//  Created by Digital on 18/02/22.
//

import SwiftUI

class DashBoardPresenter: ObservableObject{
    private let router = DashBoardRouter()
    
    func clickGeoView() -> some View {
        return router.getGeoView()
    }
    
    func clickApiView() -> some View {
        return router.getApisView()
    }
    
}
