//
//  GeoInteractor.swift
//  cibanco_arquetipo_ios
//
//  Created by Digital on 24/02/22.
//

import SwiftUI

class GeoInteractor: ObservableObject {
    
    @ObservedObject var location = LocationUtil()
    
    func getLocation()->LocationUtil{
        return location
    }
    
    func getStatus()->String{
        return location.statusString
    }
    
}
