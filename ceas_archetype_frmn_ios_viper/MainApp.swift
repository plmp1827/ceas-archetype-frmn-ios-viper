//
//  cibanco_arquetipo_iosApp.swift
//  cibanco_arquetipo_ios
//
//  Created by Digital on 18/02/22.
//

import SwiftUI
import Firebase

@main
struct MainApp: App {
    
    @Environment(\.scenePhase) private var scenePhase
    @State var isBack = false
    
    init(){
        FirebaseApp.configure()
    }

    var body: some Scene {
        WindowGroup {
            DashBoardView(presenter: DashBoardPresenter())
        }.onChange(of: scenePhase) { v in
            if v == .active {
                let window  = UIApplication.shared.windows.last!
                window.viewWithTag(221122)?.removeFromSuperview()
            }else if v == .inactive{
                if(!self.isBack){
                    let window  = UIApplication.shared.windows.last!
                    let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
                    let blurEffectView = UIVisualEffectView(effect: blurEffect)
                    blurEffectView.frame = window.frame
                    blurEffectView.tag = 221122
                    window.addSubview(blurEffectView)
                }else{
                    isBack.toggle()
                }
            }else if v == .background{
                isBack.toggle()
            }
        }
    }
}
