//
//  DashBoardRouter.swift
//  cibanco_arquetipo_ios
//
//  Created by Digital on 18/02/22.
//

import SwiftUI

class DashBoardRouter {
    
    func getGeoView() -> some View{
        let interactor = GeoInteractor()
        let presenter = GeoPresenter(interactor: interactor)
        return GeoView(presenter: presenter)
    }
    
    func getApisView() -> some View {
        let interactor = ApiInteractor()
        let presenter = ApiPresenter(interactor: interactor)
        return ApisView(presenter: presenter)
    }
    
}
